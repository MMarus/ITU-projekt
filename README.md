Welcome to share.it
==================
share.it - real time collaborative whiteboard.
Presentation of the project https://docs.google.com/presentation/d/1_mLpoLJ7_hVH-olzXcVue4Lysjl3mY1Fb5DKbrUs3G8/edit?usp=sharing

This project is written in Node.js and uses frameworks Express.js, Socket.io and Paper.js.


![alt text]( "Image1")


What you need to install the project:
-------------------
Node.js 4.2+

Paper.js (This one is a little bit painfull, since you need some Cahiro libraries to get it work)

Installation:
-------------------
```bash
git clone https://gitlab.com/Marek1349/ITU-projekt.git
cd ITU-projekt
npm install
```

### In case you have problem installing Nodejs:
https://nodejs.org/en/download/package-manager/#debian-and-ubuntu-based-linux-distributions

### Guide how to get needed packages for Paper.js:
https://github.com/Automattic/node-canvas/wiki/Installation---Ubuntu-and-other-Debian-based-systems

#### Authors:
Marek Marusic, Eudard Rybar, Juraj Vida